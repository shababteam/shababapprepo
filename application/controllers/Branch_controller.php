<?php 
defined('BASEPATH') OR exit('No direct script allowed');
   class Branch_controller extends CI_Controller {
	  protected $baseFolder		=	'branch';
	  protected $table			=	'branch';
	  protected $header			=	'layout/header';
	  protected $footer			=	'layout/footer';
	  
      public function __construct() { 
         parent::__construct();       
    	$this->load->model(array('Branch_model'));
        $this->load->model(array('Login_model'));
        //for email function  
        $this->load->library('email');

        $this->load->library('session');
        
        if(empty($this->session->userdata("user_id")))
        {
        	$this->session->set_flashdata("flash",["type"=>"danger","message"=>"Session out!"]);
        	redirect('Login/index');
        }
              
      } 
   
     public function index() { 
     	
     	$num_rows=$this->db->count_all("$this->table");
     	$this->load->library('pagination');

		$config['base_url'] = base_url().'index.php/Branch_controller/index';
		$config['total_rows'] = $num_rows;
		$config['per_page'] = 15;
		
		//$config['use_page_numbers'] = TRUE;
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		
		$this->pagination->initialize($config);
     	// ******* pagiantion configrtn ends *** /////////
     
        $query = $this->db->get("$this->table",$config['per_page'],$this->uri->segment(3)); // $config['per_page'] and $offset is for pagination
        $data['records'] = $query->result(); 
		
        $this->load->helper('url'); 
        $this->load->view("$this->header");
        $this->load->view("$this->baseFolder/index",$data);
        $this->load->view("$this->footer");
        //echo "hiii"; 
     } 
     public function add_view()
     {
	 	$this->load->helper('url'); 
        $this->load->view("$this->header");
        $this->load->view("$this->baseFolder/add");
        $this->load->view("$this->footer");
	 }	
	 public function add()
	 {
	 	$branchName		= NULL;
     	$region			= NULL;
     	$city			= NULL;
     	//$state		    = NULL;
     	$address	    = NULL;
     	//$pincode		= NULL;
     	$phone		    = NULL;
     	$mobile1	    = NULL;
     	$mobile2		= NULL;
     	$fax			= NULL;
     	$email		    = NULL;
     	$userName		= NULL;
     	$password		= NULL;
     	$submit 		= NULL;
     	
     	extract($_POST);
     	$params['branchName']=	$branchName;
     	$params['region']	 =	$region;     
     	$params['city']		 =	$city;
     	//$params['state']	 =	$state;     
     	$params['address']	 =	$address;
     	//$params['pincode']	 =	$pincode; 
     	$params['phone']	 =	$phone;
     	$params['mobile1']	 =	$mobile1; 
     	$params['mobile2']	 =	$mobile2;
     	$params['fax']	 	 =	$fax;
     	$params['email']	 =	$email; 
     	
     	$params2['userType'] =	'branch'; 
     	$params2['userName'] =	$userName;
     	$params2['password'] =	$password; 
     	
     	//branch name already exists validation
     	$branchCount = $this->Branch_model->branch_exists($branchName);//echo $branchCount;die;
     	
     	//validation for username already exist
		 $result =    $this->Login_model->user_exists($userName);
		 	
		   if($result>0) {
		       
		       $_SESSION['branchName'] = $branchName;
		   	   $_SESSION['region']		=	$region; 
     		   $_SESSION['city']	 	=	$city;
     		   $_SESSION['address']	=	$address;
     		   $_SESSION['phone']	 	=	$phone;
     		   $_SESSION['mobile1']	=	$mobile1;
     		   $_SESSION['mobile2']	=	$mobile2;
     		   $_SESSION['fax']	 	=	$fax;
     		   $_SESSION['email']	 	=	$email;
		       
		   	$this->session->set_flashdata('success_msg', 'User Name is already exists');
		   	$data['records'] = $params; //print_r($data);die;
		   	redirect('Branch_controller/add_view',$data);
		   	
		   } 

                   if($branchCount>0)
		   {
		   	 $this->session->set_flashdata('success_msg', 'Branch Name is already exists');
		   	 $data['records'] = $params; //print_r($data);die;
		   	 redirect('Branch_controller/add_view',$data);
		   }


		    	
     	else if($result == 0 || $branchCount==0) {
			
		
     	if(isset($submit))
     	{		
			$res=$this->Branch_model->insertData($params);
			$params2['branchId']=$res;
			$res2=$this->Login_model->insertBranchData($params2);
			 if($res)
	         {
	         	if($res2) {
					//for mail function
			     	$subject = "Sahara Branch Login Credentials";
			     	$msg = "UserName:".$userName."\n"."Password:".$password;
			     	mail($email,$subject,$msg);
				}
	         	
			 	$this->session->set_flashdata('success_msg', 'Added successfully');
			 }
			 else{
			 	$this->session->set_flashdata('success_msg', 'Failed to add');
			 }
		}
		
		
        redirect('Branch_controller/index');
        }
	 }
	 
	 
	 public function edit_view()
	 {
	 	$data['fields']=array(
		'ID',
		'branchName',
		'region',
		'city',
		//'state',
		'address',
		//'pincode',
		'phone',
		'mobile1',
		'mobile2',
		'fax',
		'email'
		);
	 	
	 	 $this->load->helper('form'); 
         $tableId = $this->uri->segment('3'); 
         $data['condition'] = array(
         'ID'=>$tableId
         );  
         	    
         $data['results']=$this->Branch_model->getUpdateData($data);
         $this->load->view("$this->header");
         $this->load->view("$this->baseFolder/edit",$data);
         $this->load->view("$this->footer");
	 }
	  public function edit()
	 {	 
	 	$editId			= NULL;
	 	$branchName		= NULL;
     	$region			= NULL;
     	$city			= NULL;
     	//$state		    = NULL;
     	$address	    = NULL;
     	//$pincode		= NULL;
     	$phone		    = NULL;
     	$mobile1	    = NULL;
     	$mobile2		= NULL;
     	$fax			= NULL;
     	$email		    = NULL;
     	$userName		= NULL;
     	$password		= NULL;
     	$submit 		= NULL;
     	
     	extract($_POST);
     	$editId				 =	$editId;
     	$params['branchName']=	$branchName;
     	$params['region']	 =	$region;     
     	$params['city']		 =	$city;
     	//$params['state']	 =	$state;     
     	$params['address']	 =	$address;
     	//$params['pincode']	 =	$pincode; 
     	$params['phone']	 =	$phone;
     	$params['mobile1']	 =	$mobile1; 
     	$params['mobile2']	 =	$mobile2;
     	$params['fax']	 	 =	$fax;
     	$params['email']	 =	$email; 
     	 
        //branch name already exists check
     	$values = $this->Branch_model->editBranchCheck($branchName,$editId);// echo $values;die; 

        if($values>0)
		   {
		   	 $this->session->set_flashdata('success_msg', 'Branch Name is already exists');
		   	 redirect("Branch_controller/edit_view/$editId");
		   }
     	
     	else if($values==0) {
			

    	     	
     	if(isset($submit))
     	{			
			$res=$this->Branch_model->updateAction($params,$editId);
			 if($res)
	         {
			 	$this->session->set_flashdata('success_msg', 'Updated successfully');
			 }
			 else{
			 	$this->session->set_flashdata('success_msg', 'Updation failed');
			 }
		}		
        redirect('Branch_controller/index');
 }
	 }
	 public function delete() { 
         $id = $this->uri->segment('3'); 
         $res=$this->Branch_model->deleteData($id); 
         $res2=$this->Login_model->deleteBranchData($id); 
         if($res)
         {
		 	$this->session->set_flashdata('success_msg', 'Deleted successfully');
		 }
		 else{
		 	$this->session->set_flashdata('success_msg', 'Deletion failed');
		 }
        redirect('Branch_controller/index');   		
      } 
   } 
