<?php 
defined('BASEPATH') OR exit('No direct script allowed');
   class Customer_controller extends CI_Controller {
	  protected $baseFolder		=	'customer';
	  protected $table			=	'customer';
	  protected $tableCard		=	'card';
	  protected $tableInvoice	=	'point_details';
	  protected $header			=	'layout/header';
	  protected $footer			=	'layout/footer';
	  
      public function __construct() { 
         parent::__construct(); 
         $this->load->model(array('Customer_model'));
         $this->load->model(array('Card_model'));
         $this->load->model(array('Invoice_model'));
         $this->load->model(array('Redeem_model'));
         $this->load->library('session');   
    
         if(empty($this->session->userdata("user_id")))
        {
        	$this->session->set_flashdata("flash",["type"=>"danger","message"=>"Session out!"]);
        	redirect('Login/index');
        }     
      } 
   
    public function index() { //$offset is for pagination
     	
     	$num_rows=$this->db->count_all("$this->table");
     	$this->load->library('pagination');

		$config['base_url'] = base_url().'index.php/Customer_controller/index';
		$config['total_rows'] = $num_rows;
		$config['per_page'] = 15;
		//$config['num_links'] = 2;
    
		//$config['use_page_numbers'] = TRUE;
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		
		$this->pagination->initialize($config);
     	// ******* pagiantion configrtn ends *** /////////
     	 
        $query = $this->db->get("$this->table",$config['per_page'],$this->uri->segment(3)); // $config['per_page'] and $offset is for pagination
        $data['records'] = $query->result(); 
        $query = $this->db->get("$this->tableCard"); 
        $data['cardRecords'] = $query->result(); //print_r($data);
        $this->load->helper('url'); 
        $this->load->view("$this->header");
        $this->load->view("$this->baseFolder/index",$data);
        $this->load->view("$this->footer");
        //echo "hiii"; 
     }   
     public function add_view()
     {
	 	$this->load->helper('url'); 
        $this->load->view("$this->header");
        $this->load->view("$this->baseFolder/add");
        $this->load->view("$this->footer");
	 }	
	 public function add()
	 {
	 	$title   		= NULL;
     	$customerName   = NULL;
     	$customerNameArabic   = NULL;
     	$place			= NULL;
	 	$phone   		= NULL;
     	$mobile1	    = NULL;
     	$mobile2		= NULL;
     	$fileNumber	    = NULL;
     	$email			= NULL;
     	$addedDate		= NULL;
     	$submit 		= NULL;
     	
     	extract($_POST);
     	$params['title']		=	$title;
     	$params['customerName'] =	$customerName; 
     	$params['customerNameArabic'] =	$customerNameArabic; 
     	$params['place']		=	$place;
     	$params['phone'] 		=	$phone;
     	$params['mobile1']		=	$mobile1;
     	$params['mobile2'] 		=	$mobile2;  
     	$params['fileNumber']	=	$fileNumber;
     	$params['email'] 		=	$email; 
     	$params['loginId']		=	$_SESSION['user_id'];   	
     	$params['addedDate']=date('Y-m-d', strtotime($addedDate));
     	     	
     	if(isset($submit))
     	{			
			$res=$this->Customer_model->insertData($params);
			 if($res)
	         {
			 	$this->session->set_flashdata('success_msg', 'Added successfully');
			 }
			 else{
			 	$this->session->set_flashdata('success_msg', 'Failed to add');
			 }
		}
		/*$query = $this->db->get("$this->table"); 
        $data['records'] = $query->result(); 
        $query = $this->db->get("$this->tableCard"); 
        $data['cardRecords'] = $query->result(); //print_r($data);
		$this->load->view("$this->header");		
        $this->load->view("$this->baseFolder/index",$data);
        $this->load->view("$this->footer");*/
         redirect('Customer_controller/index');
	 }
	 public function edit_view()
	 {
	 	$data['fields']=array(
		'ID',
		'title',
		'customerName',
		'customerNameArabic',
		'place',
		'phone',
		'mobile1',
		'mobile2',
		'fileNumber',
		'email',
		'addedDate'
		);
	 	
	 	 $this->load->helper('form'); 
         $tableId = $this->uri->segment('3'); 
         $data['condition'] = array(
         'ID'=>$tableId
         );  
         	    
         $data['results']=$this->Customer_model->getUpdateData($data);
         $this->load->view("$this->header");
         $this->load->view("$this->baseFolder/edit",$data);
         $this->load->view("$this->footer");
	 }
	  public function edit()
	 {	 
	 	$editId			= NULL;
	 	$title   		= NULL;
     	$customerName   = NULL;
     	$customerNameArabic   = NULL;
     	$place			= NULL;
	 	$phone   		= NULL;
     	$mobile1	    = NULL;
     	$mobile2		= NULL;
     	$fileNumber	    = NULL;
     	$email			= NULL;
     	$addedDate		= NULL;
     	$submit 		= NULL;
     	
     	extract($_POST);
     	$editId					=	$editId;
     	$params['title']		=	$title;
     	$params['customerName'] =	$customerName; 
     	$params['customerNameArabic'] =	$customerNameArabic; 
     	$params['place']		=	$place;
     	$params['phone'] 		=	$phone;
     	$params['mobile1']		=	$mobile1;
     	$params['mobile2'] 		=	$mobile2;  
     	$params['fileNumber']	=	$fileNumber;
     	$params['email'] 		=	$email; 
     	$params['loginId']		=	$_SESSION['user_id'];     	
     	$params['addedDate']=date('Y-m-d', strtotime($addedDate));
     	  //  print_r($params);die; 	
     	if(isset($submit))
     	{			
			$res=$this->Customer_model->updateAction($params,$editId);
			 if($res)
	         {
			 	$this->session->set_flashdata('success_msg', 'Updated successfully');
			 }
			 else{
			 	$this->session->set_flashdata('success_msg', 'Updation failed');
			 }
		}		
       redirect('Customer_controller/index'); 
	 }
	 public function delete() { 
         $id = $this->uri->segment('3'); 
         $res=$this->Customer_model->deleteData($id); 
         $res2=$this->Card_model->deleteCardCusData($id); 
         $res3=$this->Invoice_model->deleteCusData($id); 
         $res4=$this->Redeem_model->deleteRadeemCusData($id); 
         if($res)
         {
		 	$this->session->set_flashdata('success_msg', 'Deleted successfully');
		 }
		 else{
		 	$this->session->set_flashdata('success_msg', 'Deletion failed');
		 }   		
        redirect('Customer_controller/index');
      } 
       
   } 
