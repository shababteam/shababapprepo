<?php 
//defined('BASEPATH') OR exit('No direct script allowed');
class Redeem_controller extends CI_Controller{
	protected $baseFolder		=	'redeem';
	/*protected $table1			=	'point_details';
	protected $table2			=	'customer';
	protected $table3			=	'card';*/
	protected $table4			=	'point_setting';
	protected $header			=	'layout/header';
	protected $footer			=	'layout/footer';
	  
	public function __construct(){ 
		parent::__construct(); 
		$this->load->model(array('Redeem_model')); 
		$this->load->library('session');    

                if(empty($this->session->userdata("user_id")))
        {
        	$this->session->set_flashdata("flash",["type"=>"danger","message"=>"Session out!"]);
        	redirect('Login/index');
        }           
	} 
	public function index(){	
		/*$this->load->view("$this->header");
		$this->load->view("$this->baseFolder/index");
		$this->load->view("$this->footer");*/
		//$data['results']=$this->Invoice_model->getAllData();
		
        $this->load->helper('url'); 
        $this->load->view("$this->header");
        $this->load->view("$this->baseFolder/index");
        $this->load->view("$this->footer");
	}
	
	public function search(){
		$cardNo   		= NULL;
		$customerName   = NULL;
		$phone			= NULL;
		 	
		extract($_POST);
	     	
		$data['cardId'] 	  = $cardNo;
		$data['customerName'] = $customerName;
		$data['phone'] 		  = $phone;	        
		//$data['mobile1'] 		  = $mobile1;	        
		//$data['mobile2'] 		  = $mobile2;	        
		if($data){
			$datas['results1']=$this->Redeem_model->getSearchData($data);
		}
       			
		$datas['submit']=1;
		$data = $this->load->view("$this->baseFolder/getData",$datas);		
	}
  
	public function add()
	 {
	 	$customerId   = NULL;
     	$submit       = NULL;
     	$totalCardNo  = NULL;
     	
     	$jQotp = $this->input->post('otpIdname');//echo $jQotp;die;
     	
     	$textOtp = $this->input->post('textOtp'); //echo $textOtp;die;
     	
     	extract($_POST);
     	$totCardNo				=	$totalCardNo;
     	$params['customerId']	=	$customerId;
     	$params['loginId']		=	$_SESSION['user_id'];     	
     	$params['redeemDate']	=	date('Y-m-d');//echo $totCardNo;die;
     	
     	//for sending sms as arabic or english
     	$smstype = $_POST['smstype'];
     	
     	$lastInsertCardId=$customerName=$mobile=$cardNumber=$point=$sendContent=$sendContentArabic=$customerNameArabic=$is_ok=0;
     	$data['results']=$this->Redeem_model->getSmsCustomerData($customerId);
				foreach($data['results'] as $r){
                    $cusId = $r->ID;
                    if($smstype=="smsEnglish") {
					   $customerName=$r->customerName;
					}
					if($smstype=="smsArabic") {
					$customerName=$r->customerNameArabic;
					}
					$mobile=$r->mobile1;		
				}
				//echo $mobile;die;
				 
				
		$sms['sendSms']=$this->Redeem_model->getSms(3);
				foreach($sms['sendSms'] as $q){
					if($smstype=="smsEnglish") {
						$sendContent=$q->sms;
					}
					//echo $sendContent;die;
					if($smstype=="smsArabic") {
						$sendContent=$q->smsArabic;
					}	
                  	$smsType = $q->type;  				
				} 
			$smsSendCount=$dataSavedCount=0;
                        //sms sending 
				function http_response($url,$Message1,$status = null, $wait = 3){ 
					$time = microtime(true); 
					$expire = $time + $wait; 


					// we are the parent 
					$ch = curl_init(); 
					curl_setopt($ch, CURLOPT_URL, $url); 
					curl_setopt($ch, CURLOPT_HEADER, TRUE); 
					//curl_setopt($ch, CURLOPT_NOBODY, TRUE); // remove body 
					curl_setopt($ch,CURLOPT_POST,1);
					curl_setopt($ch,CURLOPT_POSTFIELDS,$Message1);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); 
					$head = curl_exec($ch); 
					$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE); 
					curl_close($ch); 
					//return $head;
            
					if(!$head){ 
						return FALSE; 
					} 
					else{
						return TRUE;
					}
            
					/* if($status === null) 
					{ 
					if($httpCode < 400) 
					{ 
					return TRUE; 
					} 
					else 
					{ 
					return FALSE; 
					} 
					} 
					elseif($status == $httpCode) 
					{ 
					return TRUE; 
					} 
            
					return FALSE; 
					pcntl_wait($status); */
				}	
     	for($i=1;$i<=$totCardNo;$i++)
     	{     	
			$redeemPoint		="redeem".$i;
			$cardId     		="cardId".$i;
			$params['cardId']	=	$this->input->post($cardId); 
			$params['redeemPoint']	=	$this->input->post($redeemPoint);
			if($params['redeemPoint']!=0 ||$params['redeemPoint']!='')
			{
				$lastInsertCardId=$this->Redeem_model->insertData($params);
			 if($lastInsertCardId){
			 	++$dataSavedCount;
			 	$data2['results']=$this->Redeem_model->getSmsSearchData($lastInsertCardId);
			 	foreach($data2['results'] as $r){
					$cardNumber=$r->cardNumber;
					$redeemPoint=$r->redeemPoint;
								
				}
				//echo	$cardNumber."-".$redeemPoint;	die;
				
				//function for getting points in english content
				function clean1($string,$redeemPoint,$cardNumber) {
   					//echo $string; echo $cardNo;die;
   					$string1 = str_replace('@',$redeemPoint, $string);
   					$string2 = str_replace('$',$cardNumber, $string1);
   					return $string2;
				}				
				
				//function for getting points in arabic content
				function clean2($string,$redeemPoint,$cardNumber) {
   					//echo $string; echo $cardNo;die;
   					$string1 = str_replace('A',$redeemPoint, $string);
   					$string2 = str_replace('B',$cardNumber, $string1);
   					return $string2;
				}
				
				
				if($smstype=="smsEnglish") {
					//$sendContent=$sendContent." Customer Name:".$customerName.", Card No:".$cardNumber;
					//calling function clean1()
					$content = clean1($sendContent,$redeemPoint,$cardNumber);//die;
				}
				if($smstype=="smsArabic") {
					//$sendContent=$sendContent." اسم الزبون:".$customerName.", لا بطاقة:".$cardNumber;
					//calling function clean2()
					$content = clean2($sendContent,$redeemPoint,$cardNumber);//die;
				}
					
						
				
	
	         if($mobile and $sendContent){

					$url ="http://sms.git.ind.in/api/sendmsg.php";

					$Message1 = "user=bodhi&pass=bodhi@git&sender=BODHII&phone=$mobile&text=$content&priority=ndnd&stype=normal";//echo $Message1;die;
	
					$is_ok = http_response($url,$Message1);
					if($is_ok)
					{
						++$smsSendCount;
					}
				}
				
			}//redeem if	
		}
	}
	if($is_ok && $lastInsertCardId)
				{
                    date_default_timezone_set('Asia/Kathmandu');
					$LoginId = $_SESSION['user_id'];
					
					$values['sms'] = $sendContent;
					$values['receiverId'] = $cusId;
					$values['sentDate'] = date('Y-m-d');;
					$values['sentTime'] = date('H:i:s');
					$values['sentType'] = $smsType;	
					$values['loginId']	= $LoginId;		
					
					//offer name alredy exists validation
     				        $Count = $this->Redeem_model->saveData($values); //echo $Count;die;    
                                     
					$this->session->set_flashdata('success_msg', 'Point Added & SMS sent successfully');
				}
				else if($lastInsertCardId){
					$this->session->set_flashdata('success_msg','Point added successfully');
				}
				else if($is_ok){
					$this->session->set_flashdata('success_msg', 'SMS sent successfully');
				}
				else {
					$this->session->set_flashdata('success_msg', 'Failed to add');
				}
    redirect('Redeem_controller/index');
 }
	 public function calculatePoint()
	 {
	 	$amount   = NULL;
	 	extract($_POST);
	 	//$res=$this->Point_setting_model->calcPoint($amount);
	 	$query = $this->db->get("$this->table4"); 
        $data4['records'] = $query->result();
        
        foreach($data4['records'] as $res) 
        {
			$setPoint=$res->point;
			$setAmount=$res->amount;			
		}
		$x=$amount/$setAmount;
		$totalPoint=$x*$setPoint;
		echo $totalPoint; 
	 }
	public function delete() { 
         $id = $this->uri->segment('3'); 
         $res=$this->Invoice_model->deleteData($id); 
         if($res)
         {
		 	$this->session->set_flashdata('success_msg', 'Deleted successfully');
		 }
		 else{
		 	$this->session->set_flashdata('success_msg', 'Deletion failed');
		 }
        redirect('Invoice_controller/index');
      } 
      public function report(){	
		$data['results']=$this->Invoice_model->getAllData();
		
        $this->load->helper('url'); 
        $this->load->view("$this->header");
        $this->load->view("$this->baseFolder/report",$data);
        $this->load->view("$this->footer");
	}
	
	public function otpGenerate() {
		$otp   = NULL;
	 	extract($_POST);
		
	function getGUID($length = 6){ 
	   $characters = '0123456789';
	   $charactersLength = strlen($characters);
	   $randomString = '';
	   for ($i = 0; $i < $length; $i++)
	   {
	       $randomString .= $characters[rand(0, $charactersLength - 1)];
	   }
	   return $randomString;
	}//end of function getGUID
		
		function http_response($url,$Message1,$status = null, $wait = 3){ 
					$time = microtime(true); 
					$expire = $time + $wait; 


					// we are the parent 
					$ch = curl_init(); 
					curl_setopt($ch, CURLOPT_URL, $url); 
					curl_setopt($ch, CURLOPT_HEADER, TRUE); 
					//curl_setopt($ch, CURLOPT_NOBODY, TRUE); // remove body 
					curl_setopt($ch,CURLOPT_POST,1);
					curl_setopt($ch,CURLOPT_POSTFIELDS,$Message1);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); 
					$head = curl_exec($ch); 
					$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE); 
					curl_close($ch); 
					//return $head;
            
					if(!$head){ 
						return FALSE; 
					} 
					else{
						return TRUE;
					}
				}//end of function http response
		
		
			$url ="http://sms.git.ind.in/api/sendmsg.php";
			$randomNo = getGUID();
			$Message1 = "user=bodhi&pass=bodhi@git&sender=BODHII&phone=$otp&priority=ndnd&stype=normal&text= Shabab verification code :".$randomNo; //echo $url.$Message1;
			$is_ok = http_response($url,$Message1); //echo $is_ok;
			
			if($is_ok) {
				echo $randomNo;
			}
		
	} //end of otpGenerate fun





} 
