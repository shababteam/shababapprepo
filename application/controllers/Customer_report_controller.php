<?php
defined('BASEPATH') or exit ('No direct script allowed');
class Customer_report_controller extends CI_controller{

	protected $basefolder = 'report';
	protected $header	  =	'layout/header';
	protected $footer	  =	'layout/footer';

	public function __construct(){
		parent::__construct();
		$this->load->helper('url_helper');
		$this->load->model('Customer_report_model');
		$this->load->library('session');

                if(empty($this->session->userdata("user_id")))
                {
        	$this->session->set_flashdata("flash",["type"=>"danger","message"=>"Session out!"]);
        	redirect('Login/index');
                }
	}

	public function index(){
		$branchId = $this->input->post('branchId');
		$customerName = $this->input->post('customerName');
                $phoneNo	 = $this->input->post('phoneNo');
		
		$data['record']	= $this->Customer_report_model->get_customer($branchId,$customerName,$phoneNo);
		$data['cards']	= $this->Customer_report_model->get_cards();
		$data['branchs'] = $this->Customer_report_model->get_branch();

		$this->load->view("$this->header");
		$this->load->view("$this->basefolder/customer_report",$data);
		$this->load->view("$this->footer");
	}
}