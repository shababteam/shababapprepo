<?php 
defined('BASEPATH') OR exit('No direct script allowed');
class Purchase_return_controller extends CI_Controller{
	protected $baseFolder		=	'Purchase_return';
	/*protected $table1			=	'point_details';
	protected $table2			=	'customer';
	protected $table3			=	'card';*/
	protected $table4			=	'point_setting';
	protected $header			=	'layout/header';
	protected $footer			=	'layout/footer';
	  
	public function __construct(){ 
		parent::__construct(); 
		$this->load->model(array('Purchase_return_model')); 
		$this->load->library('session');

                if(empty($this->session->userdata("user_id")))
        {
        	$this->session->set_flashdata("flash",["type"=>"danger","message"=>"Session out!"]);
        	redirect('Login/index');
        }               
	} 
	public function index(){	
        $this->load->helper('url'); 
        $this->load->view("$this->header");
        $this->load->view("$this->baseFolder/index");
        $this->load->view("$this->footer");
	}
	
	public function search(){
		$cardNo   		= NULL;
		$customerName   = NULL;
		$phone			= NULL;
		 	
		extract($_POST);
	     	
		$data['cardId'] 	  = $cardNo;
		$data['customerName'] = $customerName;
		$data['phone'] 		  = $phone;	        
		//$data['mobile1'] 		  = $mobile1;	        
		//$data['mobile2'] 		  = $mobile2;	        
		if($data){
			$datas['results1']=$this->Purchase_return_model->getSearchData($data);
		}
       			
		$datas['submit']=1;
		$data = $this->load->view("$this->baseFolder/getData",$datas);		
	}
  
	public function add()
	 {
	 	$customerId   = NULL;
     	$cardId    	  = NULL;
     	$amount		  = NULL;
     	$submit       = NULL;
     	$point		  = NULL;
     	
     	extract($_POST);
		$params['customerId']	=	$customerId;
     	$params['cardId']		=	$cardId; 
     	$params['amount']		=	$amount;
     	$params['point']		=	$point;
     	$params['loginId']		=	$_SESSION['user_id'];     	
     	$params['addedDate']	=	date('Y-m-d');

        //for getting the smstype (english or arabic)
     	$smstype = $_POST['smstype'];

     	$lastInsertCardId=$customerName=$mobile=$cardNumber=$point=$sendContent=$is_ok=$Message1 =0;
     	if(isset($submit))
     	{			
			/*$res=$this->Invoice_model->insertData($params);
			 if($res)
	         {
			 	$this->session->set_flashdata('success_msg', 'Added successfully');
			 }
			 else{
			 	$this->session->set_flashdata('success_msg', 'Failed to add');
			 }*/
			 $lastInsertCardId=$this->Purchase_return_model->insertData($params);
			if($lastInsertCardId){
				$sms['sendSms']=$this->Purchase_return_model->getSms(6); 
				foreach($sms['sendSms'] as $q){
					
					if($smstype=="smsEnglish") {
						$sendContent=$q->sms;
					}
					if($smstype=="smsArabic") {
						$sendContent=$q->smsArabic;
					}	
                  	$smsType = $q->type;  				
				}
				$data['results']=$this->Purchase_return_model->getSmsSearchData($lastInsertCardId);
				foreach($data['results'] as $r){
                    $cusId = $r->ID;
                    if($smstype=="smsEnglish") {
						$customerName=$r->customerName;
					}
					if($smstype=="smsArabic") {
						$customerName=$r->customerNameArabic;
					}	
					$mobile=$r->mobile1;
					$cardNumber=$r->cardNumber;
					$point=$r->point;	//echo	$customerName."-".$cardNumber."-".$point;	die;		
				}
				
				//function for getting points in english content
				function clean1($string,$point,$cardNumber) {
   					//echo $string; echo $cardNo;die;
   					$string1 = str_replace('@',$point, $string);
   					$string2 = str_replace('$',$cardNumber, $string1);
   					return $string2;
				}				
				
				//function for getting points in arabic content
				function clean2($string,$point,$cardNumber) {
   					//echo $string; echo $cardNo;die;
   					$string1 = str_replace('A',$point, $string);
   					$string2 = str_replace('B',$cardNumber, $string1);
   					return $string2;
				}
				
				
				
				if($smstype=="smsEnglish") {
					//$sendContent=$sendContent." Customer Name:".$customerName.", Card No:".$cardNumber;	
//echo $sendContent;die;
					//calling function clean1()
					$content = clean1($sendContent,$point,$cardNumber);//die;	
				}
				if($smstype=="smsArabic") {
					//$sendContent=$sendContent." اسم الزبون:".$customerName.", لا بطاقة:".$cardNumber;	
					//calling function clean2()
					$content = clean2($sendContent,$point,$cardNumber);//die;	
				}				
				//sms sending 
				function http_response($url,$Message1,$status = null, $wait = 3){ 
					$time = microtime(true); 
					$expire = $time + $wait; 


					// we are the parent 
					$ch = curl_init(); 
					curl_setopt($ch, CURLOPT_URL, $url); 
					curl_setopt($ch, CURLOPT_HEADER, TRUE); 
					//curl_setopt($ch, CURLOPT_NOBODY, TRUE); // remove body 
					curl_setopt($ch,CURLOPT_POST,1);
					curl_setopt($ch,CURLOPT_POSTFIELDS,$Message1);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); 
					$head = curl_exec($ch); 
					$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE); 
					curl_close($ch); 
					//return $head;
            
					if(!$head){ 
						return FALSE; 
					} 
					else{
						return TRUE;
					}
				}

                   if($mobile and $sendContent){

					$url ="http://sms.git.ind.in/api/sendmsg.php";

					$Message1 = "user=bodhi&pass=bodhi@git&sender=BODHII&phone=$mobile&text=$content&priority=ndnd&stype=normal";
	
					$is_ok = http_response($url,$Message1);
				}
				//echo $Message1;die;
				//sms sending end
				if($is_ok && $lastInsertCardId)
				{
                                        date_default_timezone_set('Asia/Kathmandu');
					$LoginId = $_SESSION['user_id'];
					
					$values['sms'] = $sendContent;
					$values['receiverId'] = $cusId;
					$values['sentDate'] = date('Y-m-d');;
					$values['sentTime'] = date('H:i:s');
					$values['sentType'] = $smsType;	
					$values['loginId']	= $LoginId;		
					
					//offer name alredy exists validation
     				        $Count = $this->Purchase_return_model->saveData($values); //echo $Count;die;

					$this->session->set_flashdata('success_msg', 'Point Added & SMS sent successfully');
				}
				else if($lastInsertCardId){
					$this->session->set_flashdata('success_msg', 'Point added successfully');
				}
				else if($is_ok){
					$this->session->set_flashdata('success_msg', 'SMS sent successfully');
				}
				else{
					$this->session->set_flashdata('success_msg', 'Failed to add');
				}
			}
		}		
        redirect('Purchase_return_controller/index');
	 }
		public function calculatePoint()
	 {
	 	$point=$j=0;
	 	$amount   = NULL;
	 	extract($_POST);
	 	$query = $this->db->get("$this->table4"); 
        $data4['records'] = $query->result();
        
        if($amount!='')
        { 
        $i=0;
	        foreach($data4['records'] as $res) 
	        {		
				$amountFrom[$i]	=	$res->amountFrom;
				$amountTo[$i]	=	$res->amountTo;
				$percentage[$i]	=	$res->percentage;
				$i++;
			}
			$rowCount = $i;
			for($i=0;$i<$rowCount;$i++)
			{
				if($amount>=$amountFrom[$i]  &&  $amount<=$amountTo[$i])
				{
			   		$limit=$i;
					break;
				}
			}
			for($j=0;$j<$limit; $j++)
			{
				if($j>1)
				{
					$point = $point + (($amountTo[$j]-$amountTo[$j-1])* $percentage[$j]/100);	 
				}
				else
				{
					$point = $point + (($amountTo[$j])* $percentage[$j]/100);				
				}					
			}
				if($j>1)
				{
					$point = $point + ($amount - $amountTo[$j-1] )* ($percentage[$j]/100);
				}
				else
				{
					$point = $point + ($amount )* ($percentage[$j]/100);
				}
		}
		echo $point; 
	 }
	public function delete() { 
         $id = $this->uri->segment('3'); 
         $res=$this->Purchase_return_model->deleteData($id); 
         if($res)
         {
		 	$this->session->set_flashdata('success_msg', 'Deleted successfully');
		 }
		 else{
		 	$this->session->set_flashdata('success_msg', 'Deletion failed');
		 }
        redirect('Purchase_return_controller/report');
      } 
      public function report(){	
		$data['results']=$this->Purchase_return_model->getAllData();
		
        $this->load->helper('url'); 
        $this->load->view("$this->header");
        $this->load->view("$this->baseFolder/report",$data);
        $this->load->view("$this->footer");
	}
} 
