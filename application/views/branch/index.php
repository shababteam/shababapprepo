<script>
	
function delete_type()
{
var del=confirm("Do you Want to Delete ?");
	if(del==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}
	
</script>



<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Branch Details
        </h1>
        <ol class="breadcrumb">
            <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Branch Details</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">List of Branches</h3>
                        <span class="pull-right"><a href="<?php echo site_url(); ?>/Branch_controller/add_view" class="btn btn-primary btn-flat">Add New</a></span>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Sl No</th>
                                <th>Branch Name</th>
                                <th>City</th>
                                <th>Phone</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            
                                
                                <?php $i =$this->uri->segment(3); 
                                
                               	foreach($records as $r) {  
                                $i++;
                               ?>
                                    <tr>
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $r->branchName; ?></td>
                                        <td><?php echo $r->city; ?></td>
                                        <td><?php echo $r->phone; ?></td>
                                        <td>
                                        	<a href="<?php echo site_url(); ?>/Branch_controller/edit_view/<?php echo $r->ID; ?>" class="btn btn-facebook btn-flat">Edit</a>
                                            <a href="<?php echo site_url(); ?>/Branch_controller/delete/<?php echo $r->ID;?>" class="btn btn-danger btn-flat"  onclick="return delete_type();">Delete
					    </a>
                                        </td>
                                    </tr>
                                    <?php  }?>
                              
                            </tbody>
                        </table>
                    </div>
                </div>
                <?php 
                  	$rowCount = count($records);
                  	 ?>
                  	<!-- for pagination --->
					<div class="row" align="center">
						<?php if($rowCount!=0) { echo $this->pagination->create_links();} else {}; ?>
					</div>
					<!--end -->
            </div>
        </div>
    </section>
</div>