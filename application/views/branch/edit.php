<div class="content-wrapper">
    <section class="content-header">
        <h1>
           Branch Registration
        </h1>
        <ol class="breadcrumb">
            <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Branch Registration</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <span style="color: #ff0000" id="user"><?=  $this->session->flashdata('success_msg'); ?></span>
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Branch Registration</h3>
                    </div>
                    <!-- /.box-header -->                    
                    <div class="box-body">
                    <form action="<?php echo  site_url(); ?>/Branch_controller/edit" method="post">
                    <?php 
                    foreach($results as $r){  										
					?>
					<input type="hidden" name="editId" value="<?php echo $r['ID'];?>">
                      <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="form-group">
                                        <label for="title">Branch Name</label> <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="Branch Name" name="branchName" id="branchName" required value="<?php echo $r['branchName'];?>">
                                    </div>
                               </div>
                               <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="form-group">
                                        <label for="title">Region</label> 
                                        <input type="text" class="form-control" placeholder="region" name="region" id="region" value="<?php echo $r['region'];?>">
                                    </div>
                               </div>
                               <div class="col-lg-4 col-md-4 col-sm-4">
                                     <label for="title">City</label> 
                                        <input type="text" class="form-control" placeholder="city" name="city" id="city" value="<?php echo $r['city'];?>"> 
                               </div>
                              </div>
                              <div class="row">                               
                               <!--<div class="col-lg-4 col-md-4 col-sm-4">
                                    <label for="title">State</label> 
                                        <input type="text" class="form-control" placeholder="state" name="state" id="state" value="<?php echo $r['state'];?>">
                                 </div>   -->                           
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                   <label for="title">Address</label> 
                                        <textarea name="address" class="form-control" placeholder="Address"><?php echo $r['address'];?></textarea>
                                  </div>
                                  <!--<div class="col-lg-4 col-md-4 col-sm-4">    
                                    <div class="form-group">
                                        <label for="title">Pincode</label> 
                                        <input type="text" class="form-control" placeholder="Pincode" name="pincode" id="pincode" value="<?php echo $r['pincode'];?>">
                                    </div> 
                                </div>-->
                                <div class="col-lg-4 col-md-4 col-sm-4"> 
                                    <div class="form-group">
                                        <label for="title">Phone</label> 
                                        <input type="text" class="form-control" placeholder="phone" name="phone" id="phone" value="<?php echo $r['phone'];?>">
                                    </div>  
                               		</div>
                               		<div class="col-lg-4 col-md-4 col-sm-4">  
                                    <div class="form-group">  
                                    <label for="title">Fax</label> 
                                        <input type="text" class="form-control" placeholder="Fax" name="fax" id="fax" value="<?php echo $r['fax'];?>">
                                    </div>
                                   </div>  
                               </div>
                              <div class="row">
                                   
                               	 <div class="col-lg-4 col-md-4 col-sm-4"> 
                                    <div class="form-group">
                                        <label for="title">Mobile 1</label> 
                                        <input type="text" class="form-control" placeholder="Mobile 1" name="mobile1" id="mobile1" value="<?php echo $r['mobile1'];?>">
                                    </div>
                                    </div>  
                                    <div class="col-lg-4 col-md-4 col-sm-4"> 
                                    <div class="form-group">   
                                    <label for="title">Mobile 2</label> 
                                        <input type="text" class="form-control" placeholder="Mobile 2" name="mobile2" id="mobile2" value="<?php echo $r['mobile2'];?>">
                                        </div>
                                    </div>    
                                    
                                    <div class="col-lg-4 col-md-4 col-sm-4" > 
                                    <div class="form-group">   
                                     <label for="title">Email<span class="text-danger">*</span></label> 
                                        <input type="email" class="form-control" placeholder="Email" name="email" id="email" value="<?php echo $r['email'];?>" required>
                                    </div>  
                                    </div>                                                                  
                                 </div>
                                 <!--end of row--> 
                                
                                 <div class="row">
                                 	<div class="col-md-12 col-lg-12 col-sm-12">
                                 		<div class="form-group">
                                        	<button type="submit" class="btn btn-primary btn-flat" name="submit">Save</button>
                                    	</div>
                                 	</div>
                                 </div>
                                   <?php } ?>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
    </section>
    </div>