<div class="content-wrapper">
    <section class="content-header">
        <h1>
           Branch Registration
        </h1>
        <ol class="breadcrumb">
            <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Branch Registration</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Branch Registration</h3>
                    </div>
                    <!-- /.box-header -->                    
                    <div class="box-body">
                    <form action="<?php echo site_url(); ?>/Branch_controller/add" method="post" onsubmit="return valid()" >
                      <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="form-group">
                                        <label for="title">Branch Name</label> <span class="text-danger">*</span>
                                        <input type="text" class="form-control" placeholder="Branch Name" name="branchName" id="branchName" required value="<?php if(isset($_SESSION['branchName'])){ echo $_SESSION['branchName'];}?>">
                                    </div>
                               </div>
                               <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="form-group">
                                        <label for="title">Region</label> 
                                        <input type="text" class="form-control" placeholder="region" name="region" id="region" value="<?php if(isset($_SESSION['region'])){ echo $_SESSION['region'];}?>">
                                    </div>
                               </div>
                               <div class="col-lg-4 col-md-4 col-sm-4">
                                     <label for="title">City</label> 
                                        <input type="text" class="form-control" placeholder="city" name="city" id="city" value="<?php if(isset($_SESSION['city'])){ echo $_SESSION['city'];}?>"> 
                               </div>
                              </div>
                              <div class="row">                               
                               <!--<div class="col-lg-4 col-md-4 col-sm-4">
                                    <label for="title">State</label> 
                                        <input type="text" class="form-control" placeholder="state" name="state" id="state">
                                 </div>     -->                         
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                   <label for="title">Address</label> 
                                        <textarea name="address" class="form-control" placeholder="Address"><?php if(isset($_SESSION['address'])){ echo $_SESSION['address'];}?></textarea>
                                  </div>
                                  <div class="col-lg-4 col-md-4 col-sm-4"> 
                                    <div class="form-group">
                                        <label for="title">Phone</label> 
                                        <input type="text" class="form-control" placeholder="phone" name="phone" id="phone" value="<?php if(isset($_SESSION['phone'])){ echo $_SESSION['phone'];}?>">
                                    </div>  
                               		</div>
                               		<div class="col-lg-4 col-md-4 col-sm-4">  
                                    <div class="form-group">  
                                    <label for="title">Fax</label> 
                                        <input type="text" class="form-control" placeholder="Fax" name="fax" id="fax" value="<?php if(isset($_SESSION['fax'])){ echo $_SESSION['fax'];}?>">
                                    </div>
                                   </div> 
                                  <!--<div class="col-lg-4 col-md-4 col-sm-4">    
                                    <div class="form-group">
                                        <label for="title">Pincode</label> 
                                        <input type="text" class="form-control" placeholder="Pincode" name="pincode" id="pincode">
                                    </div> 
                                </div>-->
                               </div>
                              <div class="row">
                                   
                               	 <div class="col-lg-4 col-md-4 col-sm-4"> 
                                    <div class="form-group">
                                        <label for="title">Mobile 1</label> 
                                        <input type="text" class="form-control" placeholder="Mobile 1" name="mobile1" id="mobile1" value="<?php if(isset($_SESSION['mobile1'])){ echo $_SESSION['mobile1'];}?>">
                                    </div>
                                    </div>  
                                    <div class="col-lg-4 col-md-4 col-sm-4"> 
                                    <div class="form-group">   
                                    <label for="title">Mobile 2</label> 
                                        <input type="text" class="form-control" placeholder="Mobile 2" name="mobile2" id="mobile2" value="<?php if(isset($_SESSION['mobile2'])){ echo $_SESSION['mobile2'];}?>">
                                        </div>
                                    </div>    
                                     
                                    <div class="col-lg-4 col-md-4 col-sm-4" > 
                                    <div class="form-group">   
                                     <label for="title">Email<span class="text-danger">*</span></label> 
                                        <input type="email" class="form-control" placeholder="Email" name="email" id="email" required value="<?php if(isset($_SESSION['email'])){ echo $_SESSION['email'];}?>">
                                    </div>  
                                    </div>                                                                  
                                 </div>
                                 <div class="row"> 
                                 <div class="col-lg-4 col-md-4 col-sm-6">
                             		<div class="form-group">
                                        <label for="title">User Name<span class="text-danger">*</span></label> 
                                        <input type="text" class="form-control" placeholder="User Name" name="userName" id="userName" required onfocus="clearbox('user')">
                                    </div>
                                    <span style="color: #ff0000" id="user"><?=  $this->session->flashdata('success_msg'); ?></span>
                             	</div>
                             	<div class="col-lg-4 col-md-4 col-sm-6">
                             		<div class="form-group">
                                        <label for="title">Password<span class="text-danger">*</span></label> 
                                        <input type="password" class="form-control" placeholder="Password" name="password" id="password" required onfocus="clearbox('pswd')">
                                        <div  id="pswd" style="height:20px; width:200px;color:#FF0000;"></div>
                                    </div>
                             	</div>
                             	<div class="col-lg-4 col-md-4 col-sm-6">
                             		<div class="form-group">
                                        <label for="title">Confirm Password<span class="text-danger">*</span></label> 
                                        <input type="password" class="form-control" placeholder="cPassword" name="cPassword" id="cPassword" required onfocus="clearbox('cpswd')">
                                        <div  id="cpswd" style="height:20px; width:500px;color:#FF0000;"></div>
                                    </div>
                             	</div>
                             </div><!--end of row--> 
                                
                                 <div class="row">
                                 	<div class="col-md-12 col-lg-12 col-sm-12">
                                 		<div class="form-group">
                                        	<button type="submit" class="btn btn-primary btn-flat" name="submit">Save</button>
                                    	</div>
                                 	</div>
                                 </div>
                                  
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
    </section>
    </div>
    <script>
function valid()
{
flag=false;
	
	jPassword=document.getElementById('password').value;
	jConfirmPassword=document.getElementById('cPassword').value;
		
		if(jPassword=="")
		{																			///for password
		document.getElementById('pswd').innerHTML="You can't leave this empty.";
		flag=true;
		}
			
		if(jConfirmPassword=="" || jConfirmPassword!=jPassword)
		{																			///for password
		document.getElementById('cpswd').innerHTML="Password And Confirm Password are Not Equal";
		flag=true;
		}	
	
	if(flag==true)
	{
	return false;
	}
																				
}
//clear the validation msg

function clearbox(Element_id)
{
document.getElementById(Element_id).innerHTML="";
}
</script>