<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Offer Details
        </h1>
        <ol class="breadcrumb">
            <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Offer Details</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header clearfix">
                        <div class="table_filter_wrapper clearfix">
                            <form class="table_filters clearfix" method="POST" action="<?php echo site_url(); ?>/OfferController/report_index">
                                <input class="form-control" name="offerName" placeholder="Offer Name" value="<?= (isset($_REQUEST['offerName']) && $_REQUEST['offerName']!= "") ? $_REQUEST['offerName'] : "" ?>" type="text">
                                <input class="form-control datepicker" name="offerDate" placeholder="Date" value="<?= (isset($_REQUEST['offerDate']) && $_REQUEST['offerDate']!= "") ? $_REQUEST['offerDate'] : "" ?>" type="text">
                                <button class="btn btn-flat btn-success" name="submit" type="submit"><i class="ion ion-search"></i></button>
                            </form>
                        </div>
                    </div>

<?php if(isset($_REQUEST['submit'])) { ?>
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Sl No</th>
                                <th>Offer</th>
                                <th>Offer Description</th>
                                <th>Date</th>
                                <!--<th>Action</th>-->
                            </tr>
                            </thead>
                            <tbody>
                                 
                                <?php if(empty($records)) { ?>
                             <tr>
                             	<td colspan="4" align="center"><h5>No Data Found.!</h5></td>
                             </tr>
                            <?php } else {?>
                                
                                <?php $i = 1; 
                                
                               	foreach($records as $r) {  
                               
                               ?>
                                    <tr>
                                        <td><?php echo $i++; ?></td>
                                        <td><?php echo $r->offer; ?></td>
                                        <td><?php echo $r->offerDescription; ?></td>
                                        <td><?php echo $r->offerSetDate; ?></td>
                                        <!--<td>
                                        	<a href="<?php echo site_url(); ?>/OfferController/edit_view/<?php echo $r->ID; ?>" class="btn btn-facebook btn-flat">Edit</a>
                                            <a href="<?php echo site_url(); ?>/OfferController/delete/<?php echo $r->ID; ?>" onclick="return delete_type()" class="btn btn-danger btn-flat">Delete
                                            </a>
                                        </td>-->
                                    </tr>
                                    <?php  }?>
                              
                            </tbody>
                           <?php } ?>
                        </table>
                    </div>
                  <?php } ?>
                </div>
            </div>
        </div>
    </section>
</div>