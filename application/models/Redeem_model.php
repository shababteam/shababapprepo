<?php 
   class Redeem_model extends CI_Model {
   	protected $table			=	'redeem';
	protected $table1			=	'point_details';
	protected $table2			=	'customer';
	protected $table3			=	'card';
        protected $table4			=	'sms_send_details';
      function __construct() { 
         parent::__construct(); 
      }
      
	   	function getSearchData($data){
	   		$cardId			=	$data['cardId'];
	        $customerName	=	$data['customerName'];
	        $phone			=	$data['phone'];
	        //$mobile1		=	$data['mobile1'];
	        //$mobile2		=	$data['mobile2'];
	        
	        /*if($cardId)
			{
				$this->db->where("card.cardId = '$cardId'");
			}
			
			if($customerName && $phone)
			{
				$this->db->where("customer.customerName='$customerName'");
				$this->db->where("customer.phone='$phone'");
			}
			if($customerName && $mobile1) {
				$this->db->where("customer.customerName='$customerName'");
				$this->db->where("customer.mobile1='$mobile1'");
			}
			if($customerName && $mobile2) {
				$this->db->where("customer.customerName='$customerName'");
				$this->db->where("customer.mobile2='$mobile2'");
			}
			
			if($cardId && $customerName && $phone) {
				$this->db->where("card.cardId='$cardId'");
				$this->db->where("customer.customerName='$customerName'");
				$this->db->where("customer.phone='$phone'");
			}
			if($cardId && $customerName && $mobile1) {
				$this->db->where("card.cardId='$cardId'");
				$this->db->where("customer.customerName='$customerName'");
				$this->db->where("customer.mobile1='$mobile1'");
			}
			if($cardId && $customerName && $mobile2) {
				$this->db->where("card.cardId='$cardId'");
				$this->db->where("customer.customerName='$customerName'");
				$this->db->where("customer.mobile2='$mobile2'");
			}*/
		    
		    $this->db->select('card.*,customer.*,customer.ID as customerId,card.ID as card_id ');
			$this->db->from('card');
			$this->db->join('customer','card.customerId = customer.ID');
			
			if($customerName && $phone)
			{
				$where = "((customer.customerName='$customerName' AND customer.phone=$phone) OR (customer.customerName='$customerName' AND customer.mobile1=$phone) OR (customer.customerName='$customerName' AND customer.mobile2=$phone))";
				
			}


                        if($phone && !$customerName)
			{
				$where = "(customer.phone='$phone' OR customer.mobile1='$phone' OR customer.mobile2='$phone')";
			}

			
			if($cardId)
			{
			
				$where = "(card.cardId=$cardId)";
			}

                        if($customerName && $phone && $cardId)
			{
				$where = "((customer.customerName='$customerName' AND customer.phone=$phone AND card.cardId=$cardId) OR (customer.customerName='$customerName' AND customer.mobile1=$phone AND card.cardId=$cardId) OR (customer.customerName='$customerName' AND customer.mobile2=$phone AND card.cardId=$cardId))";
			}
			
			$this->db->where($where);
			
			
			$this->db->order_by('card.ID');
			$query = $this->db->get();
			//echo $this->db->last_query();
			return $query->result();
	    }
	
		function pointSum($cardId){
	   		
		    $this->db->select('sum(point)as totalpoint');
			$this->db->from('point_details');
			
			$this->db->where('point_details.cardId', $cardId);			
			$query = $this->db->get();
			//echo $this->db->last_query();
			$data = $query->row();
			if($data)
			{
				$result = $data->totalpoint;
			}
			else{
				$result = "0";
				}
			return $result;
	    }
	    function redeemSum($cardId){
	   		
		    $this->db->select('sum(redeemPoint)as totalredeem');
			$this->db->from('redeem');
			
			$this->db->where('redeem.cardId', $cardId);			
			$query = $this->db->get();
			//echo $this->db->last_query();die;
			$data = $query->row();
			if($data)
			{
				$result = $data->totalredeem;
			}
			else{
				$result = "0";
				}
			return $result;
	    }
            function purchaseReturnSum($cardId){
	   		
		    $this->db->select('sum(point)as totalPurReturn');
			$this->db->from('purchase_return');
			
			$this->db->where('purchase_return.cardId', $cardId);			
			$query = $this->db->get();
			//echo $this->db->last_query();die;
			$data = $query->row();
			if($data)
			{
				$result = $data->totalPurReturn;
			}
			else{
				$result = "0";
				}
			return $result;
	    }
	 public function insertData($params)
   	{
		$ins	=	$this->db->insert($this->table,$params);
		$lastInsertId = $this->db->insert_id();
		return $lastInsertId;
	}
	public function getAllData()
	{
		$this->db->select('card.*,customer.*,point_details.*,customer.ID as customerId,card.ID as card_id,point_details.ID as point_details_id ');
		$this->db->from('card');
		$this->db->join('customer','card.customerId = customer.ID');
		$this->db->join('point_details','point_details.cardId = card.ID');
		
		$this->db->order_by('point_details.ID');
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result();
	}
	 public function deleteData($id) { 
         if ($this->db->delete($this->table1, "ID = ".$id)) { 
            return true; 
         } 
      } 
     //for deleting the customer details of particular customer that we select
      public function deleteRadeemCusData($id) { 
         if ($this->db->delete($this->table, "customerId = ".$id)) { 
            return true; 
         } 
      } 
      	function getSms($id){
	   			        
		    $this->db->select('sms_setting.sms,sms_setting.smsArabic,sms_setting.type');
			$this->db->from('sms_setting');
			$this->db->where('sms_setting.type', $id);	
						
			$query = $this->db->get();
			//echo $this->db->last_query();
			return $query->result();
	    }  	
	    function getSmsCustomerData($customerId)
	    {
			$this->db->select('customer.*');
			$this->db->from('customer');			
			$this->db->where('customer.ID', $customerId);
						
			$query = $this->db->get();
			//echo $this->db->last_query();die;
			return $query->result();
		}
	    function getSmsSearchData($lastInsertCardId){
	   			        
		    $this->db->select('card.*,redeem.*,card.cardId as cardNumber');
			$this->db->from('card');
			$this->db->join('redeem','redeem.cardId = card.ID');			
			$this->db->where('redeem.ID', $lastInsertCardId);
						
			$query = $this->db->get();
			//echo $this->db->last_query();die;
			return $query->result();
	    }
     
          public function saveData($params) { 
         $ins	=	$this->db->insert($this->table4,$params);
		 return $ins;
      } 
   } 
