<?php 
   class Card_model extends CI_Model {
	protected $table='card';
        protected $table2='sms_send_details';
      function __construct() { 
         parent::__construct(); 
      } 
   	
   	public function insertData($params)
   	{
		$ins	=	$this->db->insert($this->table,$params);
		$lastInsertId = $this->db->insert_id();
		return $lastInsertId;
	}
	 public function deleteData($id) {
         if ($this->db->delete($this->table, "ID = ".$id)) { 
            return true; 
         } 
      }
     //for deleting the card details of particular customer that we select
      public function deleteCardCusData($id) { 
         if ($this->db->delete($this->table, "customerId = ".$id)) { 
            return true; 
         } 
      }
      
       //validation for checking the cardno is existing or not
      function card_exists($cardId)
		{
		    $this->db->where('cardId',$cardId);
		    $query = $this->db->get('card'); //echo  $query;die;
		    $num = $query->num_rows(); //echo $num;
		    return $num;
		} 
function getSmsSearchData($customerId,$lastInsertCardId){
	   			        
		    $this->db->select('card.*,customer.*,customer.ID as customerId,card.ID as card_id');
			$this->db->from('card');
			$this->db->join('customer','card.customerId = customer.ID');
			
				$this->db->where('customer.ID', $customerId);			
				$this->db->where('card.ID', $lastInsertCardId);
						
			$query = $this->db->get();
			//echo $this->db->last_query();
			return $query->result();
	    }
	    function getSms($id){
	   			        
		    $this->db->select('sms_setting.sms,sms_setting.smsArabic,sms_setting.type');
			$this->db->from('sms_setting');
			$this->db->where('sms_setting.type', $id);	
						
			$query = $this->db->get();
			//echo $this->db->last_query();
			return $query->result();
	    }   

             public function saveData($params) { 
            $ins	=	$this->db->insert($this->table2,$params);
		 return $ins;
      } 
    
      public function updateAction($editId,$params)
	 {
	 	//echo $editId;die;
	 	$condition=array('ID'=>$editId);
	 	$this->db->where($condition);
		$up	=	$this->db->update($this->table,$params);
		//echo $this->db->last_query();
		return $up;
	 }

    
   } 
